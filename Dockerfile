FROM nginx:alpine

#Remove base configuration for nginx
RUN rm /etc/nginx/conf.d/*

#Copy files needed to run the application
COPY nginx.conf /etc/nginx/conf.d/

#Copy the website root
COPY . /usr/share/nginx/html